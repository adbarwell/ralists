.PHONY: all clean compile console types typecheck test
REBAR = ./rebar3

all: compile

clean:
	@$(REBAR) clean
	@rm -f erl_crash.dump

compile: src/*.erl
	@$(REBAR) compile

console:
	@$(REBAR) shell

types: compile
	@typer src/*.erl

typecheck:
	@$(REBAR) dialyzer

test:
	@$(REBAR) eunit

cover: test
	@open .eunit/index.html

src/*.erl:
	$(error Nothing to compile)
