ralists
=====

An Erlang implementation of Chris Okasaki's Random Access Lists, translated from
the Haskell Data.RandomAccessLists package.

Build
-----

    $ make compile
