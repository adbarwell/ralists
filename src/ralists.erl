-module(ralists).

%% Imports
-include("../include/std.hrl").
-include("../include/ralists.hrl").

%% Exports
-export([foldr/3, foldl/3, ramap/2, cbmap/2]).
-export([empty/0, singleton/1, replicate/2]).
-export([null/1, len/1, member/2, index/2]).
-export([head/1, tail/1, uncons/1, view/1, racons/2, append/2]).
-export([lookup/2, update/3, adjust/3, adjust_lookup/3]).
-export([filter/2, partition/2, split/2, sort/1, sortwith/2,
         zip/2, zipwith/3, unzip/1]).
-export([from_list/1, to_list/1, to_indexed_list/1]).

%% -----------------------------------------------------------------------------
%% Util

-spec foldr(fun(), any(), ralist()) -> any().
foldr(F, Init, Xs) ->
    foldrv(F, Init, view(Xs)).

-spec foldl(fun((any(), any()) -> any()), any(), ralist()) -> ralist().
foldl(F, Init, Xs) ->
    foldlv(F, Init, view(Xs)).

foldrv(_, Init, 'empty') ->
    Init;
foldrv(F, Init, {'cons', X, Xs}) ->
    F(X, foldr(F, Init, Xs)).

foldlv(_, Init, 'empty') ->
    Init;
foldlv(F, Init, {'cons', X, Xs}) ->
    foldl(F, F(X, Init), Xs).

-spec ramap(fun((any()) -> any()), ralist()) -> ralist().
ramap(F, {_S, _Xs} = Lst) ->
    ramapv(F, view(Lst)).
%%     ?ralist(S, lists:map(fun({W, T}) -> {W, cbmap(F, T)} end, Xs)).

ramapv(_, 'empty') ->
    empty();
ramapv(F, {'cons', X, Xs}) ->
    racons(F(X), ramap(F, Xs)).

-spec cbmap(fun((any()) -> any()), cbtree()) -> cbtree().
cbmap(F, {'leaf', X}) ->
    ?leaf(F(X));
cbmap(F, {'node', X, L, R}) ->
    ?cnode(F(X), cbmap(F, L), cbmap(F, R)).

%% -----------------------------------------------------------------------------
%% Construction

-spec empty() -> ralist().
empty() ->
    {0, []}.

-spec singleton(any()) -> ralist().
singleton(X) ->
    {1, [{1, ?leaf(X)}]}.

-spec replicate(integer(), any()) -> ralist().
replicate(N, X) ->
    from_list(lists:duplicate(N, X)).

%% -----------------------------------------------------------------------------
%% Query

-spec null(ralist()) -> boolean().
null({0, _}) ->
    true;
null(_) ->
    false.

-spec len(ralist()) -> integer().
len({S, _}) ->
    S.

-spec member(any(), ralist()) -> boolean().
member(X, Ys) ->
    case index(X, Ys) of
        'nothing' ->
            false;
        _ ->
            true
    end.

-spec index(any(), ralist()) -> integer() | 'nothing'.
index(X, {_, Ys}) ->
    index(X, Ys, 0).

index(_, [], _) ->
    'nothing';
index(X, [{W, T} | Ts], Acc) ->
    case index_tree(W, X, T) of
        'nothing' ->
            index(X, Ts, Acc + W);
        I when is_integer(I) ->
            Acc + I
    end.

index_tree(_, X, {'leaf', E}) ->
    if
        X =:= E -> 0;
        true    -> 'nothing'
    end;
index_tree(W, X, {'node', E, L, R}) ->
    if
        X =:= E -> 0;
        true    ->
            HalfW = W div 2,
            case index_tree(HalfW, X, L) of
                I when is_integer(I) ->
                    I + 1;
                _Otherwise ->
                    case index_tree(HalfW, X, R) of
                        I when is_integer(I) ->
                            I + 1 + HalfW;
                        _Otherwise ->
                            'nothing'
                    end
            end
    end.


%% -----------------------------------------------------------------------------
%% List specific operations

-spec head(ralist()) -> any().
head(Xs) ->
    element(1, uncons(Xs)).

-spec tail(ralist()) -> any().
tail(Xs) ->
    element(2, uncons(Xs)).

-spec uncons(ralist()) -> {any(), ralist()}.
uncons(Xs) ->
    case view(Xs) of
        'empty' ->
            error("ralist: empty list");
        {'cons', Y, Ys} ->
            {Y, Ys}
    end.

-spec view(ralist()) -> raview().
view({S, Xs}) ->
    case Xs of
        [] ->
            ?empty;
        [{_, {'leaf', X}} | Ts] ->
            ?vcons(X, ?ralist(S-1, Ts));
        [{W, {'node', X, L, R}} | Ts] ->
            HalfW = W div 2,
            ?vcons(X, ?ralist(S-1, [{HalfW, L}, {HalfW, R} | Ts]))
    end.

-spec racons(any(), ralist()) -> ralist().
racons(X, {S, List}) ->
    {S+1,
     case List of
         [{S1, T1}, {S2, T2} | Xs] = Xss ->
             if
                 S1 =:= S2 -> [{1+2*S1, ?cnode(X, T1, T2)} | Xs];
                 true      -> [{1, ?leaf(X)} | Xss]
             end;
         Xs ->
             [{1, ?leaf(X)} | Xs]
     end}.

-spec append(ralist(), ralist()) -> ralist().
append(Xs, Ys) ->
    foldr(fun(X, Zs) -> racons(X, Zs) end, Ys, Xs).

%% -----------------------------------------------------------------------------
%% Random-access specific operations

-spec lookup(integer(), ralist()) -> any().
lookup(I, Xs) ->
    element(1, adjust_lookup(fun(X) -> X end, I, Xs)).

-spec update(integer(), any(), ralist()) -> ralist().
update(I, X_new, Xs) ->
    element(2, adjust_lookup(fun(_) -> X_new end, I, Xs)).

-spec adjust(fun((any()) -> any()), integer(), ralist()) -> ralist().
adjust(F, I, Xs) ->
    element(2, adjust_lookup(F, I, Xs)).

-spec adjust_lookup(fun((any()) -> any()),
                    integer(),
                    ralist()) -> {any(), ralist()}.
adjust_lookup(_, I, {S, []}) ->
    error({"Index Out of Bounds", {I, S}});
adjust_lookup(F, I, {S, Xs}) ->
    {X, Xs1} = update_lookup(F, I, Xs),
    {X, ?ralist(S, Xs1)}.

update_lookup(_, I, []) ->
    error({"Index Out of Bounds", I});
update_lookup(F, I, [{W, T} | Ts]) when I < W ->
    {X, T1} = adjust_lookup_tree(F, W, I, T),
    {X, [{W, T1} | Ts]};
update_lookup(F, I, [{W, _T} = Hd | Ts]) ->
    {X, Ts1} = update_lookup(F, I-W, Ts),
    {X, [Hd | Ts1]}.

-spec adjust_lookup_tree(fun((any()) -> any()),
                         integer(),
                         integer(),
                         cbtree()) -> {any(), cbtree()}.
adjust_lookup_tree(F, 1, 0, {'leaf', X}) ->
    {X, ?leaf(F(X))};
adjust_lookup_tree(_, _, _, {'leaf', _}) ->
    error("Index Out of Bounds");
adjust_lookup_tree(F, W, I, {'node', X, L, R}) ->
    HalfW = W div 2,
    if
        I =:= 0 ->
            {X, ?cnode(F(X), L, R)};
        I =< HalfW ->
            {X1, L1} = adjust_lookup_tree(F, HalfW, I-1, L),
            {X1, ?cnode(X, L1, R)};
        true ->
            {X1, R1} = adjust_lookup_tree(F, HalfW, I - 1 - HalfW, R),
            {X1, ?cnode(X, R, R1)}
    end.

%% -----------------------------------------------------------------------------
%% Miscellaneous

-spec filter(fun((any()) -> boolean()), ralist()) -> ralist().
filter(P, Xs) ->
    element(1, partition(P, Xs)).

-spec partition(fun((any()) -> boolean()), ralist()) -> {ralist(), ralist()}.
partition(P, Xs) ->
    case view(Xs) of
        'empty' ->
            {empty(), empty()};
        {'cons', Y, Ys} ->
            {Yes, No} = partition(P, Ys),
            case P(Y) of
                true ->
                    {racons(Y, Yes), No};
                false ->
                    {Yes, racons(Y, No)}
            end
    end.

-spec split(integer(), ralist()) -> {ralist(), ralist()}.
split(0, Xs) ->
    {empty(), Xs};
split(N, Xs) ->
    case view(Xs) of
        'empty' ->
            {Xs, empty()};
        {'cons', Y, Ys} ->
            {L, R} = split(N-1, Ys),
            {racons(Y, L), R}
    end.

-spec sort(ralist()) -> ralist().
sort(Xs) ->
    sortwith(fun(X, Y) -> X =< Y end, Xs).

%% Implemented as merge sort. This could get rather nasty with very long lists;
%% consider some way of limiting descent.
-spec sortwith(fun((any(), any()) -> boolean()), ralist()) -> ralist().
sortwith(_, {0, _}) ->
    empty();
sortwith(_, {1, _} = Xs) ->
    Xs;
sortwith(F, {S, _} = Xs) ->
    {L, R} = split(S div 2, Xs),
    merge(F, sortwith(F, L), sortwith(F, R)).

merge(F, Xs, Ys) ->
    case view(Xs) of
        'empty' ->
            Ys;
        {'cons', X, Xs1} ->
            case view(Ys) of
                'empty' ->
                    Xs;
                {'cons', Y, Ys1} ->
                    case F(X, Y) of
                        true ->
                            racons(X, merge(F, Xs1, Ys));
                        false ->
                            racons(Y, merge(F, Xs, Ys1))
                    end
            end
    end.

-spec zip(ralist(), ralist()) -> ralist().
zip(Xs, Ys) ->
    zipwith(fun(X, Y) -> {X, Y} end, Xs, Ys).

-spec zipwith(fun((any(), any()) -> any()), ralist(), ralist()) -> ralist().
zipwith(F, {S, As}, {S, Bs}) ->
    ?ralist(S, lists:zipwith(fun({W, Xs}, {_, Ys}) ->
                                     {W, zipwithtree(F, Xs, Ys)}
                             end, As, Bs));
zipwith(F, As, Bs) ->
    case {view(As), view(Bs)} of
        {{'cons', X, Xs}, {'cons', Y, Ys}} ->
            racons(F(X, Y), zipwith(F, Xs, Ys));
        _ ->
            empty()
    end.

-spec zipwithtree(fun((any(), any()) -> any()), cbtree(), cbtree()) -> cbtree().
zipwithtree(F, {'leaf', X}, {'leaf', Y}) ->
    ?leaf(F(X, Y));
zipwithtree(F, {'node', X, LX, RX}, {'node', Y, LY, RY}) ->
    ?cnode(F(X, Y), zipwithtree(F, LX, LY), zipwithtree(F, RX, RY));
zipwithtree(_, _, _) ->
    error("Invalid skew binary number representation").

-spec unzip(ralist()) -> {ralist(), ralist()}.
unzip({S, Xs}) ->
    {As, Bs} = unzip_1(Xs),
    {?ralist(S, As), ?ralist(S, Bs)}.

unzip_1([]) ->
    {[], []};
unzip_1([{W, T} | Ts]) ->
    {TA, TB} = unziptree(T),
    {TsA, TsB} = unzip_1(Ts),
    {[{W, TA} | TsA], [{W, TB} | TsB]}.

-spec unziptree(cbtree()) -> {cbtree(), cbtree()}.
unziptree({'leaf', {X, Y}}) ->
    {?leaf(X), ?leaf(Y)};
unziptree({'node', {X, Y}, L, R}) ->
    {XL, YL} = unziptree(L),
    {XR, YR} = unziptree(R),
    {?cnode(X, XL, XR), ?cnode(Y, YL, YR)}.

%% -----------------------------------------------------------------------------
%% Conversion -- List

-spec from_list([any()]) -> ralist().
from_list(Xs) ->
    lists:foldr(fun(X, Acc) ->
                        racons(X, Acc)
                end, ralists:empty(), Xs).

-spec to_list(ralist()) -> [any()].
to_list(Xs) ->
    foldr(fun(X, Acc) -> [X | Acc] end, [], Xs).

-spec to_indexed_list(ralist()) -> [{integer(), any()}].
to_indexed_list(Xs) ->
    lists:zip(lists:seq(0, len(Xs)-1), to_list(Xs)).
