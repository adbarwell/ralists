%% A number of repeated functions/macros used across erlang programs

%%------------------------------------------------------------------------------
%% Debugging

-ifndef(debug).
-define(debug, true).
%% -define(debug, false).
-endif.

-ifndef(print).
-define(print(Var),
	io:format("~p:~p~n  ~p: ~p~n", [?MODULE, ?LINE, ??Var, Var])).
-define(print_eunit(Var),
	io:format(user, "~p:~p~n  ~p: ~p~n", [?MODULE, ?LINE, ??Var, Var])).
-define(print_ints(Var),
	io:format("~p:~p~n  ~p: ~w~n", [?MODULE, ?LINE, ??Var, Var])).
-define(print_char(Var),
	io:format("~p:~p~n  ~p: ~c~n", [?MODULE, ?LINE, ??Var, Var])).
-define(print_dict(Var),
	io:format("~p:~p~n  ~p: ~w~n", [?MODULE, ?LINE, ??Var,
					lists:sort(dict:to_list(Var))])).
-define(print_set(Var),
	io:format("~p:~p~n  ~p: ~w~n", [?MODULE, ?LINE, ??Var,
					lists:sort(sets:to_list(Var))])).
-define(error(Reason),
	io:format("Error: ~p:~p~n  ~p: ~p~n", [?MODULE, ?LINE, ??Reason, Reason])).
-define(err_enoent(FName),
        io:format("Error: ~p:~p~n  File Not Found: ~s~n",
                  [?MODULE, ?LINE, FName])).
-endif.

-ifndef(timelvl).
%% -define(timelvl, seconds).
-define(timelvl, milli_seconds).
%% -define(timelvl, micro_seconds).

-ifndef(now).
-define(now, erlang:system_time(?timelvl)).
-ifndef(nowdiff).
-define(nowdiff(T1, T2), T2 - T1).
-ifndef(pnowdiff).
-define(pnowdiff(T1, T2), ?print(?nowdiff(T1, T2))).

-endif.
-endif.
-endif.
-endif.
