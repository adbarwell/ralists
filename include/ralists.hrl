
%% Type declarations
-type ralist() :: {integer(), [{integer(), cbtree()}]}.
-type cbtree() :: {'leaf', any()} | {'node', any(), cbtree(), cbtree()}.

-type raview() :: 'empty' | {'cons', any(), ralist()}.

%% Macros

-define(ralist(N, T), {N, T}).
-define(leaf(X), {'leaf', X}).
-define(cnode(X, T1, T2), {'node', X, T1, T2}).

-define(empty, 'empty').
-define(vcons(Hd, Tl), {'cons', Hd, Tl}).
